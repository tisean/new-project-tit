package by.ita.je;

import static org.junit.Assert.*;

public class FirstServiceTest {

    @org.junit.Test
    public void numberOfCharInWord() {
        FirstService firstService = new FirstService();
        Integer actual = firstService.numberOfCharInWord("Hi");
        Integer expected = 2;
        assertEquals(expected, actual);
    }
}